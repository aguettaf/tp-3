import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Window {
	
	private String seq;
	private int n;
	
	//les getters
	public String getseq() {
		return this.seq;
	}
	public int getn () {
		return this.n;
	}
	
	//les constucteurs (polymorphisme)
	public Window (String s) {
		this.seq=s;
		this.n=s.length();
	}
	
	//constructeur aleatoire
	//generer un p:
	Random alea=new Random ();
	
	
	public Window (int len, double p) {
		//creation dune séquence
		String [] array= {"C","G","A","T"};//liste a partir duquel on prend les bases
		List<String> liste = new ArrayList<String>();//liste vide
	for (int i=0;i<len;i++) {
		if (p<0.5) {
			int numeroAleatoire= alea.nextInt(1-2);
			String paireDeBases=array[numeroAleatoire];
			liste.add( paireDeBases );
		}else if(0.5<p && p<0.75) {
			liste.add( array[3] );
		}else {
			liste.add( array[4]);
		}
	}
	String séquenceAleatoire=liste.toString();
	this.seq=séquenceAleatoire;
	this.n=séquenceAleatoire.length();
}	
	
	//methode calcule p (pourcentage de cg dans la premiere séquence)
		public double percentageGC() {
		String [] seqString= seq.split("");
		int nombreDeCG=0;
		for (int j=0;j<n;j++) {
			if (seqString[j].equals("C")||seqString[j].equals("G")) {nombreDeCG++;}
		}
		double p=nombreDeCG/n;
		return p;
		}
	
		
		//toString methode
		public String toString () {
			return "The current sequence, of length" + this.getn() + ", has a percentage CG of" + this.percentageGC() ;
		}
		
			
	
	
	
	
	public static void main(String[] args) {
		
		
		
	}

}
